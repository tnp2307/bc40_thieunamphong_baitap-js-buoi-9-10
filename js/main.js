var dsnv = [];
const DSNV = "DSNV";
const TBTKNV = "tbTKNV";
const TBTEN = "tbTen";
const TBEMAIL = "tbEmail";
const TBMATKHAU = "tbMatKhau";
const TBNGAY = "tbNgay";
const TBLUONG = "tbLuongCB";
const TBCHUCVU = "tbChucVu";
const TBGIOLAM = "tbGiolam";
var dsnvJson = localStorage.getItem(DSNV);
if (dsnvJson != null) {
  var nvArray = JSON.parse(dsnvJson);
  dsnv = nvArray.map(function (item) {
    return (nhanVien = new NhanVien(
      item.tkNV,
      item.hoVaTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCB,
      item.chucVu,
      item.gioLam
    ));
  });
}
renderdsnv(dsnv);

// Them nhan vien function
function themNV() {
  var nhanVien = layThongTinForm();

  // validate
  var isValid = true;
  var isValidtknv = true;
  var isValidTen = true;
  var isValidEmail = true;
  var isValidMatKhau = true;
  var isValidNgayLam = true;
  var isValidLuong = true;
  var isValidChucVu = true;
  var isValidGioLam = true;

  isValidtknv =
    kiemTraTrong(nhanVien.tkNV, TBTKNV) &&
    kiemTraTrung(nhanVien.tkNV, dsnv, TBTKNV) &&
    kiemTraDoDai(nhanVien.tkNV, TBTKNV, 4, 6) &&
    kiemTraSo(nhanVien.tkNV, TBTKNV);
  isValidTen =
    kiemTraTrong(nhanVien.hoVaTen, TBTEN) &&
    kiemTraChu(nhanVien.hoVaTen, TBTEN);

  isValidEmail =
    kiemTraTrong(nhanVien.email, TBEMAIL) &&
    kiemTraEmail(nhanVien.email, TBEMAIL);
  isValidMatKhau =
    kiemTraTrong(nhanVien.matKhau, TBMATKHAU) &&
    kiemTraPass(nhanVien.matKhau, TBMATKHAU) &&
    kiemTraDoDai(nhanVien.matKhau, TBMATKHAU, 6, 10);

  isValidNgayLam =
    kiemTraNgayLam(nhanVien.ngayLam, TBNGAY) &&
    kiemTraTrong(nhanVien.ngayLam, TBNGAY);

  isValidLuong =
    kiemTraTrong(nhanVien.luongCB, TBLUONG) &&
    kiemTraSo(nhanVien.luongCB, TBLUONG) &&
    kiemTraMinMax(nhanVien.luongCB, TBLUONG, 1000000, 20000000);
  isValidChucVu = kiemTraTrong(nhanVien.chucVu, TBCHUCVU);
  isValidGioLam =
    kiemTraTrong(nhanVien.gioLam, TBGIOLAM) &&
    kiemTraSo(nhanVien.gioLam, TBGIOLAM) &&
    kiemTraMinMax(nhanVien.gioLam, TBGIOLAM, 80, 200);

  isValid =
    isValidtknv &
    isValidEmail &
    isValidTen &
    isValidMatKhau &
    isValidNgayLam &
    isValidLuong &
    isValidChucVu &
    isValidGioLam;
  if (isValid) {
    dsnv.push(nhanVien);
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, dsnvJson);
    renderdsnv(dsnv);
  } else {
  }
}

// xoa nhan vien

function xoa(id) {
  var viTri = timViTri(id, dsnv);
  console.log(viTri);
  if (viTri != -1) {
    dsnv.splice(viTri, 1);
    renderdsnv(dsnv);
  }
}

function suaNV(id) {
  var viTri = timViTri(id, dsnv);
  if (viTri != -1) {
    var nhanVien = dsnv[viTri];
    $("#myModal").modal("show");
    document.getElementById("tknv").value = nhanVien.tkNV;
    document.getElementById("name").value = nhanVien.hoVaTen;
    document.getElementById("email").value = nhanVien.email;
    document.getElementById("password").value = nhanVien.matKhau;
    document.getElementById("datepicker").value = nhanVien.ngayLam;
    document.getElementById("luongCB").value = nhanVien.luongCB;
    document.getElementById("chucvu").value = nhanVien.chucVu;
    document.getElementById("gioLam").value = nhanVien.gioLam;
  }
}

function capNhat() {
  console.log("hello");
  var nhanVien = layThongTinForm();

  // validate
  var isValid = true;
  var isValidtknv = true;
  var isValidTen = true;
  var isValidEmail = true;
  var isValidMatKhau = true;
  var isValidNgayLam = true;
  var isValidLuong = true;
  var isValidChucVu = true;
  var isValidGioLam = true;

  isValidtknv =
    kiemTraTrong(nhanVien.tkNV, TBTKNV) &&
    kiemTraDoDai(nhanVien.tkNV, TBTKNV, 4, 6) &&
    kiemTraSo(nhanVien.tkNV, TBTKNV);
  isValidTen =
    kiemTraTrong(nhanVien.hoVaTen, TBTEN) &&
    kiemTraChu(nhanVien.hoVaTen, TBTEN);

  isValidEmail =
    kiemTraTrong(nhanVien.email, TBEMAIL) &&
    kiemTraEmail(nhanVien.email, TBEMAIL);
  isValidMatKhau =
    kiemTraTrong(nhanVien.matKhau, TBMATKHAU) &&
    kiemTraPass(nhanVien.matKhau, TBMATKHAU) &&
    kiemTraDoDai(nhanVien.matKhau, TBMATKHAU, 6, 10);

  isValidNgayLam =
    kiemTraNgayLam(nhanVien.ngayLam, TBNGAY) &&
    kiemTraTrong(nhanVien.ngayLam, TBNGAY);

  isValidLuong =
    kiemTraTrong(nhanVien.luongCB, TBLUONG) &&
    kiemTraSo(nhanVien.luongCB, TBLUONG) &&
    kiemTraMinMax(nhanVien.luongCB, TBLUONG, 1000000, 20000000);
  isValidChucVu = kiemTraTrong(nhanVien.chucVu, TBCHUCVU);
  isValidGioLam =
    kiemTraTrong(nhanVien.gioLam, TBGIOLAM) &&
    kiemTraSo(nhanVien.gioLam, TBGIOLAM) &&
    kiemTraMinMax(nhanVien.gioLam, TBGIOLAM, 80, 200);

  isValid =
    isValidtknv &
    isValidEmail &
    isValidTen &
    isValidMatKhau &
    isValidNgayLam &
    isValidLuong &
    isValidChucVu &
    isValidGioLam;
  if (isValid) {
    var viTri = timViTri(nhanVien.tkNV, dsnv);
    console.log(viTri);
    if (viTri != -1) {
      dsnv[viTri] = nhanVien;
      renderdsnv(dsnv);
    }
  }
}
