function NhanVien(
  _tkNV,
  _hoVaTen,
  _email,
  _matKhau,
  _dateOfWork,
  _luongCB,
  _chucVu,
  _gioLam
) {
  this.tkNV = _tkNV;
  this.hoVaTen = _hoVaTen;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _dateOfWork;
  this.luongCB = _luongCB * 1;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tinhTongLuong = function () {
    switch (this.chucVu) {
      case "Sếp":
        return this.luongCB * 3;
      case "Trưởng phòng":
        return this.luongCB * 2;
      case "Sếp":
        return this.luongCB * 1;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam>=192){
      return "Xuất sắc"
    }else if(this.gioLam>=176){
      return "Giỏi"
    }else if (this.gioLam>= 160){
      return "Khá"
    }else{
      return "Trung bình"
    }
  };
}
