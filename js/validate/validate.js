function kiemTraTrung(id, nvArr, idErr) {
  var index = nvArr.findIndex(function (item) {
    return id == item.tkNV;
  });
  if (index == -1) {
    document.getElementById(idErr).innerText = "";
    document.getElementById(idErr).classList.add("d-none");
    return true;
  } else {
    document.getElementById(idErr).innerText = "Tài khoản nhân viên trùng";
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}

function kiemTraDoDai(value, idErr, min, max) {
  var length = value.length;
  if (length < min || length > max) {
    document.getElementById(
      idErr
    ).innerText = `Số lượng ký tự từ ${min} tới ${max}`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function kiemTraSo(value, idErr) {
  var reg = /^\d+$/;
  var isNumber = true;
  isNumber = reg.test(value);
  if (isNumber) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = `Trường phải là số`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}
function kiemTraChu(value, idErr) {
  var reg = /\p{Letter}/gu;
  var isLetter = true;
  isLetter = reg.test(value);
  if (isLetter) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = `Trường phải là chữ`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}

function kiemTraTrong(value, idErr) {
  var reg = /\S+/;
  var isBlank = true;
  isBlank = reg.test(value);
  if (isBlank) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = `Trường không được trống`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}
function kiemTraEmail(value, idErr) {
  var reg =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var isEmail = true;
  isEmail = reg.test(value);
  if (isEmail) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = `Trường phải là email`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}

function kiemTraPass(value, idErr) {
  var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$/;

  var isPass = true;
  isPass = reg.test(value);
  if (isPass) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(
      idErr
    ).innerText = `Trường phải có ít nhất 1 số, 1 ký tự đặc biệt, 1 chữ in hoa`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}
function kiemTraNgayLam(value, idErr) {
  var reg =
    /^(?:(0[1-9]|1[012])[\/.](0[1-9]|[12][0-9]|3[01])[\/.](19|20)[0-9]{2})$/;

  var isDate = true;
  isDate = reg.test(value);
  if (isDate) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(
      idErr
    ).innerText = `Ngày Làm phải có format MM/DD/YYYY`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  }
}

function kiemTraMinMax(value, idErr, min, max) {
  var luongCB = value;
  if (luongCB < min || luongCB > max) {
    document.getElementById(
      idErr
    ).innerText = `Lương cơ bản phải trong khoảng từ ${min} tới ${max}`;
    document.getElementById(idErr).classList.add("d-block");
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

