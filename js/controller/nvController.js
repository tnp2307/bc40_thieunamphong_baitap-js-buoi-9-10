// lay thong tin tư form
function layThongTinForm() {
  var _tkNV = document.getElementById("tknv").value;
  var _hoVaTen = document.getElementById("name").value;
  var _email = document.getElementById("email").value;
  var _matKhau = document.getElementById("password").value;
  var _dateOfWork = document.getElementById("datepicker").value;

  // var _dateOfWork = new Date($("#datepicker").val());
  // var day = _dateOfWork.getDate();
  // var month = _dateOfWork.getMonth() + 1;
  // var year = _dateOfWork.getFullYear();
  // _dateOfWork = [day, month, year].join("/");
  var _luongCB = document.getElementById("luongCB").value * 1;
  var _chucVu = document.getElementById("chucvu").value;
  var _gioLam = document.getElementById("gioLam").value;
  var nhanVien = new NhanVien(
    _tkNV,
    _hoVaTen,
    _email,
    _matKhau,
    _dateOfWork,
    _luongCB,
    _chucVu,
    _gioLam
  );
  return nhanVien;
}

// render dsvn vao bang
function renderdsnv(nvArray) {
  var contentHTML = "";

  for (var i = 0; i < nvArray.length; i++) {
    var item = nvArray[i];
    contentHTML += `<tr>
    <td>${item.tkNV}</td>
    <td>${item.hoVaTen}</td>
    <td>${item.email}</td>
    <td>${item.ngayLam}</td>
    <td>${item.chucVu}</td>
    <td>${item.tinhTongLuong()}</td>
    <td>${item.xepLoai()}</td>
    <td><div class="d-inline-flex">
      <button class="btn btn-danger mr-1"
      onclick="xoa('${item.tkNV}')">Xóa</button>
      <button class="btn btn-warning"
      onclick="suaNV('${item.tkNV}')">Sửa</button>
    </div></td>
    </tr>
    `;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function timViTri(id, nvArray) {
  var viTri = -1;
  for (var index = 0; index < nvArray.length; index++) {
    var nv = nvArray[index];
    if (nv.tkNV == id) {
      viTri = index;
      break;
    }  
  }
  return viTri;
}
